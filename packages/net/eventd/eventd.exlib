# Copyright 2011-2016 Quentin “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

OPTION_RENAMES=(
    'avahi dns-sd'
    'espeak plugins:tts'
    'libnotify plugins:libnotify'
    'libcanberra plugins:libcanberra'
    'notification-daemon plugins:notification-daemon'
    'purple plugins:im'
    'sndfile plugins:sound'
)

require github [ user=sardemff7 ] systemd-service
require meson [ meson_minimum_version=0.47.0 ]
require option-renames

export_exlib_phases src_test

SUMMARY="Small daemon to act on remote or local events"
HOMEPAGE="https://www.eventd.org"

LICENCES="
    GPL-3 [[ note = [ For eventd, eventdctl, and plugins ] ]]
    LGPL-3 [[ note = [ For libeventd, libeventc, libeventc-light and libeventd-plugin ] ]]
    MIT [[ note = [ For eventc and included git submodules, libnkutils and libgwater ] ]]
"
SLOT="0"

MYOPTIONS="
    debug                 [[ description = [ Extra debug output ] ]]
    dns-sd                [[ description = [ DNS-SD support (publish and browse) through Avahi ] ]]
    fbcon                 [[ description = [ Framebuffer backend for notification plugin ] ]]
    gobject-introspection
    ssdp                  [[ description = [ SSDP support (public and browse) through GSSDP ] ]]
    systemd
    wayland               [[ description = [ Wayland backend for notification plugin ] ]]
    websocket             [[ description = [ EvP protocol over WebSocket through libsoup ] ]]
    X                     [[ description = [ XCB backend for notification plugin ] ]]

    ( plugins:
        im                    [[ description = [ IM plugin through libpurple ] ]]
        libnotify
        libcanberra           [[ description = [ Themes and files sound support through libcanberra ] ]]
        notification-daemon   [[ description = [ Graphical notifications plugin ] ]]
        sound                 [[ description = [ sound file plugin through libsndfile ] ]]
        tts                   [[ description = [ Text-to-Speech plugin through Speech Dispatcher ] ]]
        webhook               [[ description = [ WebHook sending plugin through libsoup ] ]]
    )

    ( fbcon wayland X ) [[ *requires = [ plugins: notification-daemon ] ]]
    plugins:notification-daemon? ( ( fbcon wayland X ) [[ number-selected = at-least-one ]] )
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.5
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        virtual/pkg-config
        wayland? ( sys-libs/wayland-wall )
    build+run:
        dev-libs/glib:2[>=2.40.0]
        sys-apps/util-linux [[ note = [ For libuuid ] ]]
        dns-sd? ( net-dns/avahi )
        gobject-introspection? (
            gnome-desktop/gobject-introspection:1[>=1.42.0]
        )
        ssdp? ( net-libs/gssdp:1.0 )
        systemd? (
            user/eventd
            group/eventd
            sys-apps/systemd[>=36]
        )
        wayland? ( sys-libs/wayland[>=1.9.91] )
        websocket? ( gnome-desktop/libsoup:2.4 )
        X? (
            x11-libs/libxcb
            x11-utils/xcb-util
            x11-utils/xcb-util-wm
        )

        plugins:im? ( net-im/pidgin )
        plugins:libcanberra? (
            media-libs/libcanberra
        )
        plugins:libnotify? (
            x11-libs/gdk-pixbuf:2.0
        )
        plugins:notification-daemon? (
            x11-libs/cairo[>=1.12][X?]
            x11-libs/gdk-pixbuf:2.0
            x11-libs/libxkbcommon[>=0.4.1][X?]
            x11-libs/pango
        )
        plugins:sound? (
            media-libs/libsndfile
            media-sound/pulseaudio
        )
        plugins:tts? ( app-speech/speechd )
        plugins:webhook? ( gnome-desktop/libsoup:2.4[>=2.42] )
    run:
        dev-libs/glib-networking[ssl(+)]
        plugins:notification-daemon? (
            gnome-desktop/librsvg:2 [[ description = [ SVG support for notification plugin ] ]]
            x11-apps/xkeyboard-config
        )
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    dns-sd
    debug
    'fbcon nd-fbdev'
    gobject-introspection
    ssdp
    systemd
    'wayland nd-wayland'
    websocket
    'X nd-xcb'

    plugins:im
    plugins:libnotify
    plugins:libcanberra
    plugins:notification-daemon
    plugins:sound
    plugins:tts
    plugins:webhook
)

export EVENTD_TESTS_TMP_DIR=${TEMP}

eventd_src_test() {
    esandbox allow_net unix:"${TEMP}/**"
    meson_src_test
    esandbox disallow_net unix:"${TEMP}/**"
}

